from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .solicitudDetailView import SolicitudDetailView
from .solicitudCreatedView import SolicitudCreatedView
