from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.solicitudes import Solicitudes
from authApp.serializers.solicitudesSerializer import SolicitudesSerializer
from rest_framework_simplejwt.backends import TokenBackend

class SolicitudDetailView(generics.RetrieveAPIView):
    queryset = Solicitudes.objects.all() # Modelo
    serializer_class = SolicitudesSerializer # Serializador

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)

        if valid_data['username'] != kwargs['pk']: # Verifica si el usuario que solicita la inf corresponde con el token
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        return super().get(request,*args,*kwargs)