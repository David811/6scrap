from django.conf import settings
from rest_framework import  status, views
from rest_framework.response import Response
from authApp.serializers.solicitudesSerializer import SolicitudesSerializer
from rest_framework_simplejwt.backends import TokenBackend

class SolicitudCreatedView(views.APIView):
    def post(self,request,*args, **kwargs):
       
    
        token = request.META.get('HTTP_AUTHORIZATION')[7:] # pide el token
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False) # decodifica el token 

        request.data["user_id"]= valid_data["user_id"]  # agrega el ID al diccionario
        print(request.data,"POST") # print para saber que info se le envia al serializador
  
        serializer = SolicitudesSerializer(data=request.data) # envia el diccionario al serializador

        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.validated_data,status=status.HTTP_201_CREATED)

#mtictut9fi_bog@unal.edu.co