from django.db.models import fields
from authApp.models.solicitudes import Solicitudes
from rest_framework import serializers
from authApp.models.user import User


class SolicitudesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitudes
        fields = ["id",
                  "user_id",
                  "fecha_recoleccion",
                  "hora_recoleccion",
                  "direccion_recoleccion",
                  "tipo_material",
                  "descripcion",
                  "programacion_mensual"
                  ]
    
    
    def create(self,validated_data):
        print(validated_data,"CREATE") # Print para ver que informacion le llego al serializador

        #instanceUser =User.objects.get(id=validated_data["user_id"])

        solicitud = Solicitudes.objects.create(user_id=1,**validated_data) # METODO que funciono para enviar la solicitud
                                                                            # pero con usuario fijo
        #solicitud = Solicitudes.objects.create(**validated_data)
       
        return solicitud

    def to_representation(self, obj):
        solicitud = Solicitudes.objects.get(id=obj.id)
        return {
            "id":solicitud.id,
            "user_id":solicitud.user,
            "sol_fecha_recoleccion":solicitud.fecha_recoleccion,
            "sol_hora_recoleccion":solicitud.hora_recoleccion,
            "sol_direccion":solicitud.direccion_recoleccion,
            "sol_tipo_material":solicitud.tipo_material,
            "sol_descripcion":solicitud.descripcion,
            "sol_programacion_mensual":solicitud.programacion_mensual
        }

""" def update(self, instance, validated_data):

        instance.id=validated_data.get("id",instance.id)
        instance.user=validated_data.get("user",instance.user)
        instance.fecha_recoleccion=validated_data.get("fecha_recoleccion",instance.fecha_recoleccion)
        instance.hora_recoleccion=validated_data.get("hora_recoleccion",instance.hora_recoleccion)
        instance.direccion_recoleccion=validated_data.get("direccion_recoleccion",instance.direccion_recoleccion)
        instance.tipo_material=validated_data.get("tipo_material",instance.tipo_material)
        instance.descripcion=validated_data.get("descripcion",instance.descripcion)
        instance.programacion_mensual=validated_data.get("programacion_mensual",instance.programacion_mensual)
        
        instance.save()

        return instance"""


#scadavidg@unal.edu.co
