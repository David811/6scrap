from rest_framework import serializers
#from authApp.models import solicitudes
from authApp.models.user import User
#from authApp.models.solicitudes import Solicitudes
#from authApp.serializers.solicitudesSerializer import SolicitudesSerializer


class UserSerializer(serializers.ModelSerializer):
    #solicitudes = SolicitudesSerializer()

    class Meta:
        model = User
        fields = ['id',
                  'username',
                  'password',
                  'name',
                  'identificacion',
                  'last_name',
                  'empresa',
                  'email',
                  'celular',
                  'direccion']

# Borrado lo referente a acoount!!!
def create(self, validated_data):
    userInstance = User.objects.create(**validated_data)
    return userInstance



def to_representation(self, obj):
    user = User.objects.get(id=obj.id)
    return {
        'id': user.id,
        'username': user.username,
        'name': user.name,
        'last_name': user.last_name,
        'identificacion': user.identificacion,
        'email': user.email,
        'celular': user.celular,
        'empresa': user.empresa,
        'direccion': user.direccion
    }
