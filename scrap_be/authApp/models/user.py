#---creado
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password
from django.db import models

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username=username,
            password=password,
            )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('username', max_length = 31, unique=True)
    password = models.CharField('user_password', max_length = 256)
    name = models.CharField('user_nombres', max_length = 32)
    identificacion = models.BigIntegerField("user_identificacion")
    last_name = models.CharField('user_apellidos', max_length = 33)
    empresa= models.BooleanField("user_empresa",default=False)
    email = models.EmailField('user_correo', max_length = 100)
    celular = models.CharField("user_celular",max_length = 34)
    direccion = models.CharField("user_direccion",max_length=135)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
    objects = UserManager()
    USERNAME_FIELD = 'username'