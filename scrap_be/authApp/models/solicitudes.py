from django.db import models
from .user import User
from authApp.models import user


class Solicitudes(models.Model):
    id = models.AutoField("id", primary_key=True)
    user= models.ForeignKey(
        User, related_name="solicitudes", on_delete=models.CASCADE)
    fecha_recoleccion = models.DateField("sol_fecha_recoleccion")
    hora_recoleccion = models.TimeField("sol_hora_recoleccion")
    direccion_recoleccion = models.CharField("sol_direccion", max_length=90)
    tipo_material = models.CharField("sol_tipo_material", max_length=130)
    descripcion = models.CharField("sol_descripcion", max_length=130)
    programacion_mensual = models.BooleanField("sol_programacion_mensual")
